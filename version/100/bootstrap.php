<?php

namespace fm_nocache_cookies;

use EventDispatcher;
use Shop;

/**
 * Class Bootstrap
 * @package fm_nocache_cookies
 */
class Bootstrap extends \AbstractPlugin
{
    /**
     * @param EventDispatcher $dispatcher
     */
    public function boot(EventDispatcher $dispatcher)
    {
        parent::boot($dispatcher);

        $dispatcher->listen('shop.hook.' . HOOK_WARENKORB_CLASS_FUEGEEIN, function () {
            $this->setCookie('jtl_cart');
        });

        $dispatcher->listen('shop.hook.' . HOOK_KUNDE_CLASS_HOLLOGINKUNDE, function () {
            $this->setCookie('jtl_logged_in');
        });
    }

    /**
     * @param string $name
     */
    private function setCookie($name)
    {
        $cookieConfig = $this->getCookieConfig();
        $exp          = ($cookieConfig['lifetime'] === 0) ? 0 : time() + $cookieConfig['lifetime'];
        \setcookie(
            $name,
            '1',
            $exp, $cookieConfig['path'],
            $cookieConfig['domain'],
            $cookieConfig['secure'],
            $cookieConfig['httponly']
        );
    }

    /**
     * @return array
     */
    private function getCookieConfig()
    {
        $conf           = Shop::getSettings([CONF_GLOBAL]);
        $cookieDefaults = session_get_cookie_params();
        $lifetime       = isset($cookieDefaults['lifetime'])
            ? $cookieDefaults['lifetime']
            : 0;
        $path           = isset($cookieDefaults['path'])
            ? $cookieDefaults['path']
            : '';
        $domain         = isset($cookieDefaults['domain'])
            ? $cookieDefaults['domain']
            : '';
        $secure         = isset($cookieDefaults['secure'])
            ? $cookieDefaults['secure']
            : false;
        $httpOnly       = isset($cookieDefaults['httponly'])
            ? $cookieDefaults['httponly']
            : false;
        if (isset($conf['global']['global_cookie_secure']) && $conf['global']['global_cookie_secure'] !== 'S') {
            $secure = $conf['global']['global_cookie_secure'] === 'Y';
        }
        if (isset($conf['global']['global_cookie_httponly']) && $conf['global']['global_cookie_httponly'] !== 'S') {
            $httpOnly = $conf['global']['global_cookie_httponly'] === 'Y';
        }
        if (isset($conf['global']['global_cookie_lifetime'])
            && \is_numeric($conf['global']['global_cookie_lifetime'])
            && (int)$conf['global']['global_cookie_lifetime'] > 0
        ) {
            $lifetime = (int)$conf['global']['global_cookie_lifetime'];
        }
        if (!empty($conf['global']['global_cookie_path'])) {
            $path = $conf['global']['global_cookie_path'];
        }

        return [
            'lifetime' => $lifetime,
            'path'     => $path,
            'httponly' => $httpOnly,
            'secure'   => $secure,
            'domain'   => $domain
        ];
    }
}
